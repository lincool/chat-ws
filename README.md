# 感谢
---
本组件提取的 `ColorUI`中的头部导航栏。
 
本组件聊天部分页面 由 `聊天模板`提供

在此感谢 `@文晓港` 大佬提供的非常优秀的UI框架，推荐大家使用：[github地址](https://github.com/weilanwl/ColorUI) [插件市场地址](https://ext.dcloud.net.cn/plugin?id=239)

在此感谢 `@回梦無痕` 大佬提供的聊天模板,推荐大家使用  [插件市场地址](https://ext.dcloud.net.cn/plugin?id=324)


>一个聊天demo 正在努力完善中

>后台用go开发 菜鸟正在学习 练手项目

