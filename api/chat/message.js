import request from "@/utils/request.js";
export function getMessageListApi(data) {
    return request({
        url: "/api/chat/getMessageList",
        method: "GET",
        data: data
    });
}