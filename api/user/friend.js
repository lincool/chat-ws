import request from "@/utils/request.js";
export function getFriendList(data) {
    return request({
        url: "/api/getFriendList",
        method: "GET",
        data: data
    });
}