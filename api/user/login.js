import request from "@/utils/request.js";
export function loginByAccount(data) {
    return request({
        url: "/api/auth/login",
        method: "POST",
        data: data
    });
}