
let API_URL = "";
let UPLOAD_URL = "http://up-z2.qiniu.com";
let UPLOAD_DOMAIN = "http://youmeng.await.fun";
// APP
if (process.env.VUE_APP_PLATFORM === 'app-plus') {
    if (process.env.NODE_ENV === 'development') {
        API_URL = "http://192.168.0.206:3000";//http://192.168.0.197:8122 http://192.168.0.179:8122   http://192.168.0.179:8122
    } else {
        // 正式
        API_URL = "http://localhost:8122/";
    }
} else { // H5 模式
    if (process.env.NODE_ENV === 'development') {
        API_URL = "http://192.168.0.206:3000";//";http://192.168.0.179:8122  http://192.168.0.179:8122
    } else {
        // 正式
        API_URL = "http://api.mt.gongyitech.com:9000/";
    }
}

export {
    API_URL,
    UPLOAD_URL,
    UPLOAD_DOMAIN,
}
