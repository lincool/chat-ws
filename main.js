import App from './App'
import store from './store'
import home from './pages/home/index.vue'
Vue.component('home',home)
import friends from './pages/friends/index.vue'
Vue.component('friends',friends)
import msg from './pages/msg/index.vue'
Vue.component('msg',msg)
import my from './pages/my/index.vue'
Vue.component('my',my)
import cuCustom from './components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)
/* main.js中引用 */
import sundehengCustom from './components/sundeheng-custom/sundeheng-custom.vue'
Vue.component('sundeheng-custom',sundehengCustom)
Vue.prototype.$store = store
// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif