import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        /**
         * 是否需要强制登录
         */
        forcedLogin:false,
        hasLogin: false,
        userName: "",
		userId:'',
		homeId:'',//房间ID
		token:'',
		user:null
    },
    mutations: {
        login(state, userModel) {
			state.userName = userModel.userName || '新用户';
            state.hasLogin = true;
			state.userId=userModel.userId;
			state.token='';
			state.user=userModel.user;
			//console.log("-------1------"+state.userId);
        },
        logout(state) {
            state.userName = "";
            state.hasLogin = false;
			state.userId="";
			state.token="";
			state.user=null;
        }
    }
})
export function set(user){
	//console.log("-----------------s-----------------------");
	//store.login(user)
	//this.login(user)
}

export default store
