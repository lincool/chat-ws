/**
 * 存储localStorage
 */
export const setStorageSync = (key, val) => {
    if (!key) return;
	try {
	    uni.setStorageSync(key, val);

				//#ifdef APP-PLUS
				var setEvent={
					key:key,
					newValue:val
				}
				uni.$emit('setItemEvent',setEvent)
				//#endif
				//#ifdef H5
				let setEvent = new Event('setItemEvent')
				        setEvent.key = key
				        setEvent.newValue = val
				document.dispatchEvent(setEvent)
				//#endif
	} catch (e) {
	    // error
	}
};

/**
 * 获取localStorage
 */
export const getStorageSync = key => {
    if (!key) return;
    let val = uni.getStorageSync(key);
    return val;
};

/**
 * 删除localStorage
 */
export const removeStorageSync = key => {
    if (!key) return;
    uni.removeStorageSync(key);
};
